from architecture import Architecture
from modelParams import ModelParams

from pygame import Surface
from pygame import draw
from pygame import Rect

import numpy as np
import matrixUtils as mu
import math


class FloorField:

    def __init__(self, architecture: Architecture, model_params: ModelParams):
        self.model_params = model_params
        self.arch = architecture
        self.staticFF = np.zeros((self.arch.height, self.arch.width))
        self.proxemicsFF = np.zeros((self.arch.height, self.arch.width))
        self.wallFF = np.zeros((self.arch.height, self.arch.width))
        self.sumFF = np.zeros((self.arch.height, self.arch.width))

        self.init_static_ff()
        self.calc_static_ff()
        self.calc_wall_ff()

    def init_static_ff(self):
        for y in range(self.arch.height):
            for x in range(self.arch.width):
                if self.arch.plan[y][x] == self.model_params.floor:
                    self.staticFF[y][x] = 1000
                elif self.arch.plan[y][x] == self.model_params.wall:
                    self.staticFF[y][x] = -1
                else:
                    self.staticFF[y][x] = 0

    def calc_static_ff(self):
        to_check = []
        for y in range(self.arch.height):
            for x in range(self.arch.width):
                if self.staticFF[y][x] == 0:
                    to_check.extend(self.arch.get_cell_neighbors(y, x))
        while to_check:
            print("To check size: ", len(to_check))
            y, x = to_check.pop(0)
            old_val = self.staticFF[y][x]
            self.calc_cell_ff(y, x)
            if old_val > self.staticFF[y][x]:
                for n in self.arch.get_cell_neighbors(y,x):
                    if n not in to_check:
                        to_check.append(n)

    def calc_cell_ff(self, y, x):
        nei = self.arch.get_cell_neighbors(y, x)
        for nei_y, nei_x in nei:
            if self.staticFF[nei_y][nei_x] < 0:  # check if neighbor cell is wall
                continue
            else:
                m = self.staticFF[nei_y][nei_x]
                if abs(y - nei_y) + abs(x - nei_x) == 1:
                    m += self.model_params.direct_step_cost_cell
                else:
                    m += self.model_params.diagonal_step_cost_cell
                if m < self.staticFF[y][x]:
                    self.staticFF[y][x] = m

    def add_fields(self):
        self.sumFF = np.zeros((self.arch.height, self.arch.width))
        mu.paste(self.sumFF, self.staticFF, (0, 0))
        mu.paste(self.sumFF, self.wallFF, (0, 0))
        mu.paste(self.sumFF, self.proxemicsFF, (0, 0))

    # steering which field drives agent behavior
    def get_ff(self, y, x):
        return self.sumFF[y][x]

    def get_sff(self, y, x):
        return self.staticFF[y][x]

    def recalculate_pff(self, agent_list):
        self.proxemicsFF = np.zeros((self.arch.height, self.arch.width))
        for agent in agent_list:
            mu.paste(self.proxemicsFF, agent.get_pff(), (agent.y - agent.agent_pff_index, agent.x - agent.agent_pff_index))

    def paint_pff(self, screen: Surface):
        for y in range(self.arch.height):
            for x in range(self.arch.width):
                top_pos = self.model_params.cell_size_px * y
                left_pos = self.model_params.cell_size_px * x
                strength = min(self.proxemicsFF[y][x] * 500, 255)
                color = (0, 0, strength)
                draw.rect(screen, color, Rect(left_pos, top_pos, self.model_params.cell_size_px, self.model_params.cell_size_px))

    def calc_wall_ff(self):
        for y in range(self.arch.height):
            print("WallFF, y = ", y)
            for x in range(self.arch.width):
                self.wallFF[y][x] = self.calc_cell_wall_ff(y, x)

    def calc_cell_wall_ff(self, cell_y, cell_x):
        nearest_wall_dist = 10000
        wff_cell_range = math.ceil(self.model_params.wff_range / self.model_params.cell_size)
        for y in range(max(cell_y - wff_cell_range, 0), min(cell_y + wff_cell_range+1, self.arch.height)):
            for x in range(max(cell_x - wff_cell_range, 0), min(cell_x + wff_cell_range+1, self.arch.width)):
                if self.arch.plan[y][x] == self.model_params.wall:
                    dist = mu.distance(cell_y, cell_x, y, x)
                    if dist < nearest_wall_dist:
                        nearest_wall_dist = dist
        real_dist = nearest_wall_dist*self.model_params.cell_size - self.model_params.cell_size/2
        return self.wall_ff_from_dist(real_dist, wff_cell_range)

    def wall_ff_from_dist(self, dist, max_range):
        if dist > max_range:
            return 0
        if dist > 0.001:
            return (1/dist) * self.model_params.wff_multiplier - self.model_params.wff_multiplier
        else:
            return -1
