from agentGenerationStrategy import AgentGenerationStrategy

class ModelParams:

    def __init__(self):
        # architecture file info
        self.architecture_file = "res/plans/arch3.txt"
        self.wall = 1
        self.door = 2
        self.floor = 0
        self.architecture_cell_size = 0.4  # in meters

        # model space discretization
        self.cell_size = 0.4  # in meters

        # time calculation
        self.max_speed = 1.2  # m/s
        self.step_seconds_ratio = self.max_speed/self.cell_size

        # model params
        self.direct_step_cost_cell = 1 * self.cell_size
        self.diagonal_step_cost_cell = 1.31 * self.cell_size  # set 2*direct_step_cost_cell or more for von Neuman neighborhood for staticFF

        # Wall Floor Field params
        self.wff_range = 1  # in meters
        self.wff_multiplier = 0.03

        # agents info
        self.agent_generation_strategy = AgentGenerationStrategy.RANDOM
        self.agents_num = 70
        self.generate_agents_turns = 200  # only in case of AgentGenerationStrategy.DURING_SIMULATION_IN_AREA
        self.agents_file = "res/agents/one.txt"
        self.agent_pff_radius = 1  # in meters
        self.diagonal_movement_reduction = True
        self.inertia = False
        self.inertia_coef = 0.02
        self.injuries = True
        self.injury_limit = 2.5
        self.min_pushing_to_injury = 1

        # drawing params
        self.cell_size_px = 48
        self.agent_size_px = int(self.cell_size_px / 2)
        self.size_to_px = self.cell_size_px/self.cell_size
        self.background_color = (0, 0, 0)
        self.agent_color = (255, 0, 0)
        self.injuried_agent_color = (255, 255, 100)
        self.agent_area_color = (40, 40, 200)
        self.wall_color = (50, 50, 50)
        self.door_color = (20, 150, 20)
        self.netting_color = (100, 0, 0)

        #stats param
        self.min_sec_to_fundamental = 2





