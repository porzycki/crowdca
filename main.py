from modelParams import ModelParams
from simulation import Simulation

import matplotlib.pyplot as plt
from colorama import Fore

import time
import pygame

mp1 = ModelParams()
#mp2 = ModelParams()

#mp1.architecture_file = "res/plans/deadlock.txt"

#mp1.cell_size = 0.2  - nie można tutaj zmieniać bo nie oblicza się direct_step_cost_cell i diagonal_step_cost_cell
#mp2.cell_size = 0.2

#simList = [] na przyszłośc - lista symulacji
#sim1 = Simulation(mp1)
#sim2 = Simulation(mp2)


#screen = pygame.display.set_mode((sim1.screen_width, sim1.screen_height))

screen = pygame.display.set_mode((1400, 800))
max_fps = 20

def paint_all():
    sim1.paint(screen, 0, 0)
    #sim2.paint(screen, 700, 0)
    pygame.display.update()





def main_loop():
    done = False
    clock = pygame.time.Clock()
    positionsFile = open("results/korytarz" + "_" + str(int(sim1.model_params.cell_size*100)) + ".txt", "w")
    while not done:
        done = sim1.simulation_step()
        #done = sim2.simulation_step()
        sim1.save_agent_pos_to_file(positionsFile)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                done = True
        # !! odkomentować te dwie linie po testach
        paint_all()
        clock.tick(max_fps)
        if sim1.supervisor.turns > 5000:
            done = True
    positionsFile.close()



start = time.time()
pygame.init()
repetitions = 1
sumTurns = 0
sumCollisions = 0
for i in range(repetitions):
    sim1 = Simulation(mp1)
    main_loop()
    sumTurns = sumTurns + sim1.supervisor.turns
    sumCollisions = sumCollisions + sim1.supervisor.collisions
    plt.figure(2*i)
    if sim1.dens_speed_pairs:
        plt.scatter(*zip(*sim1.dens_speed_pairs), marker='.')
    plt.figure(2*i+1)
    plt.imshow(sim1.trajectories, cmap='jet')
pygame.quit()
end = time. time()

print(Fore.BLUE + "Avg simulation run time ", (end - start)/repetitions, " seconds.")
print(Fore.GREEN + "Evacuation time ", sumTurns/repetitions, " turns.")
print(Fore.GREEN + "There was ", sumCollisions/repetitions, " collisions between agents.")
print(Fore.GREEN + "On average ", sumCollisions/sumTurns, " collisions per turn.")
print(Fore.GREEN + "On average ", sumCollisions/(repetitions*sim1.model_params.agents_num), " collisions per agent.")

#trajektorie
plt.imshow(sim1.trajectories, cmap='jet')


#plt.imshow(sim1.floorField.proxemicsFF)

#plt.imshow(sim1.floor.plan)
'''
# matlibplot
fig, axs = plt.subplots(2, 3)
axs[0][0].set_title('Floor plan')
axs[0][0].imshow(floor.plan)

axs[0][1].set_title('Static FF')
axs[0][1].imshow(floorField.staticFF)

axs[0][2].set_title('Wall FF')
axs[0][2].imshow(floorField.wallFF)

axs[1][0].set_title('Proxemics FF')
axs[1][0].imshow(floorField.proxemicsFF)

axs[1][1].set_title('Sum FF')
axs[1][1].imshow(floorField.sumFF)

axs[1][2].set_title('Movement')

plt.show()


'''