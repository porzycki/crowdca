import math

def paste_slices(tup):
    pos, w, max_w = tup
    wall_min = max(pos, 0)
    wall_max = min(pos + w, max_w)
    block_min = -min(pos, 0)
    block_max = max_w - max(pos + w, max_w)
    block_max = block_max if block_max != 0 else None
    return slice(wall_min, wall_max), slice(block_min, block_max)


def paste(wall, block, loc):
    loc_zip = zip(loc, block.shape, wall.shape)
    wall_slices, block_slices = zip(*map(paste_slices, loc_zip))
    wall[wall_slices] += block[block_slices]


def distance(y1, x1, y2, x2):
    return math.sqrt((y1-y2)**2 + (x1-x2)**2)


def rect_area(rect):
    return (rect[2] - rect[0])*(rect[3]-rect[1])
