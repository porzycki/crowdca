from architecture import Architecture
from agent import Agent
from floorField import FloorField
from modelParams import ModelParams

from pygame import Surface

import random
from colorama import Fore


class AgentsSupervisor:

    def __init__(self, architecture: Architecture, model_params: ModelParams):
        self.model_params = model_params
        self.agents = []
        self.arch = architecture
        self.collisions = 0
        self.turns = 0  # move it to other class

    def generate_from_file(self, filename):
        with open(filename) as f:
            for line in f:
                y, x = [int(n) for n in line.split(",")]
                if not self.arch.is_floor(y, x):
                    print(Fore.YELLOW + "WARNING: Wrong agent position: ", y, ", ", x, "Agent removed.")
                    continue
                new_agent = Agent(y, x, self.model_params)
                if self.is_collision(new_agent):
                    print(Fore.YELLOW + "WARNING: Agent spawned on occupied position: ", y, ", ", x, "Agent removed.")
                    continue
                else:
                    self.agents.append(new_agent)

    def generate_random(self, agents_num):
        while len(self.agents) < agents_num:
            y = random.randint(0, self.arch.height - 1)
            x = random.randint(0, self.arch.width - 1)
            new_agent = Agent(y, x, self.model_params)
            if self.is_collision(new_agent) or not self.arch.is_floor(y, x):
                continue
            else:
                self.agents.append(new_agent)

    def generate_random_in_area(self, agents_num, y1, x1, y2, x2):
        added_agents = 0
        while added_agents < agents_num:
            y = random.randint(y1, y2)
            x = random.randint(x1, x2)
            new_agent = Agent(y, x, self.model_params)
            if self.is_collision(new_agent) or not self.arch.is_floor(y, x):
                continue
            else:
                self.agents.append(new_agent)
                added_agents +=1

   # def spawn_random_agent_in_area(self, agents_num, y1, x1, y2, x2):


    def is_collision(self, other):
        for agent in self.agents:
            if agent.collide(other):
                return True
        return False

    def paint_agents(self, screen: Surface):
        for a in self.agents:
            a.paint(screen)

    def move_agents(self, floor_field: FloorField):
        if self.model_params.injuries:
            self.calculate_injuries(floor_field)
        self.agents_decides_if_evacuate()
        evacuated_agents = []
        self.turns += 1
        for agent in self.agents:
            agent.desired_move(floor_field)
        for agent in self.agents:
            can_move = True
            for other in self.agents:
                if agent is not other and agent.will_collide(other):
                    can_move = False
                    self.collisions += 1
            if can_move:
                agent.move()
            if self.arch.is_door(agent.y, agent.x):
                evacuated_agents.append(agent)
                self.agents.remove(agent)
        return evacuated_agents

    def agents_decides_if_evacuate(self):
        for agent in self.agents:
            if not agent.is_moving:
                agent.decide_to_evacuate()

    def calculate_injuries(self, floor_field: FloorField):
        for agent in self.agents:
            agent.calculate_injuries(floor_field)

    def is_agent_list_empty(self):
        if len(self.agents) > 0:
            return False
        return True

    def num_of_agents_in_rect(self, y1, x1, y2, x2):
        num = 0
        for agent in self.agents:
            if agent.is_in_rect(y1, x1, y2, x2):
                num += 1
        return num



