from modelParams import ModelParams
from architecture import Architecture
from floorField import FloorField
from agentsSupervisor import AgentsSupervisor
from agentGenerationStrategy import AgentGenerationStrategy

import matrixUtils as mu
import math

import pygame
import numpy as np

from statistics import mean


class Simulation:

    def __init__(self, model_params: ModelParams, stats_area=(0.4, 8, 3.6, 11.6), spawn_area=(0.4, 0.4, 3.6, 4)):
        self.model_params = model_params
        self.floor = Architecture(self.model_params)
        self.floorField = FloorField(self.floor, self.model_params)

        self.supervisor = AgentsSupervisor(self.floor, self.model_params)
        self.spawn_area = spawn_area

        if self.model_params.agent_generation_strategy == AgentGenerationStrategy.RANDOM:
            self.supervisor.generate_random(self.model_params.agents_num)
        elif self.model_params.agent_generation_strategy == AgentGenerationStrategy.FROM_FILE:
            self.supervisor.generate_from_file(self.model_params.agents_file)
        elif self.model_params.agent_generation_strategy == AgentGenerationStrategy.RANDOM_IN_AREA:
            self.supervisor.generate_random_in_area(self.model_params.agents_num,
                                                    int(self.spawn_area[0]/self.model_params.cell_size),
                                                    int(self.spawn_area[1] / self.model_params.cell_size),
                                                    int(self.spawn_area[2] / self.model_params.cell_size),
                                                    int(self.spawn_area[3] / self.model_params.cell_size))

        #może to przenieść
        self.screen_height = self.floor.height * self.model_params.cell_size_px
        self.screen_width = self.floor.width * self.model_params.cell_size_px

        self.sim_screen = pygame.Surface((self.screen_width, self.screen_height))
        self.fieldsSurface = pygame.Surface((self.screen_width, self.screen_height))

        #stats
        self.trajectories = self.initialize_trajectories()
        self.densities = []
        self.sim_step_num = 0
        self.stats_area = stats_area
        self.stats_area_dist = abs(stats_area[1]-stats_area[3])  # assuming vertical corridor
        self.stats_area_size = mu.rect_area(self.stats_area)
        self.dens_speed_pairs = []


    def simulation_step(self):
        self.sim_step_num += 1
        if self.model_params.agent_generation_strategy == AgentGenerationStrategy.DURING_SIMULATION_IN_AREA:
            self.generate_agents()
        self.floorField.add_fields()
        evacuated_agents = self.supervisor.move_agents(self.floorField)
        self.floorField.recalculate_pff(self.supervisor.agents)
        self.calc_stats(evacuated_agents)
        return self.is_simulation_finished()

    def generate_agents(self):
        if self.model_params.generate_agents_turns > self.sim_step_num:
            self.supervisor.generate_random_in_area(math.ceil(self.sim_step_num/100),
                                                    int(self.spawn_area[0] / self.model_params.cell_size),
                                                    int(self.spawn_area[1] / self.model_params.cell_size),
                                                    int(self.spawn_area[2] / self.model_params.cell_size),
                                                    int(self.spawn_area[3] / self.model_params.cell_size))

    def calc_stats(self, evacuated_agents):
        self.actualize_trajectories()
        self.track_agents_in_stats_area()
        self.calc_density_in_stats_area()
        for agent in evacuated_agents:
            time = float(agent.stats_area_leave - agent.stats_area_enter) / self.model_params.step_seconds_ratio
            if time > self.model_params.min_sec_to_fundamental:
                speed = self.stats_area_dist / time
                avg_dens = self.calc_avg_density_in_frames(agent.stats_area_enter, agent.stats_area_leave+1)
                self.dens_speed_pairs.append((avg_dens, speed))


    def is_simulation_finished(self):
        return self.supervisor.is_agent_list_empty()

    def paint(self, screen: pygame.Surface, pos_y, pos_x):
        self.sim_screen.fill(self.model_params.background_color)
        self.floor.paint(self.sim_screen)
        self.floorField.paint_pff(self.fieldsSurface)
        self.supervisor.paint_agents(self.sim_screen)
        self.sim_screen.blit(self.fieldsSurface, (0, 0), special_flags=pygame.BLEND_ADD)
        screen.blit(self.sim_screen, (pos_y, pos_x))

    def initialize_trajectories(self):
        traj = np.zeros((self.floor.height, self.floor.width))
        for y in range(self.floor.height):
            for x in range(self.floor.width):
                if self.floor.plan[y][x] == self.model_params.wall:
                    traj[y][x] = -1
        return traj

    def actualize_trajectories(self):
        for a in self.supervisor.agents:
            self.trajectories[a.y][a.x] += 1

    def save_agent_pos_to_file(self, filename):
        filename.write("<data>\n")
        cell_size_cm = self.model_params.cell_size * 100
        for agent in self.supervisor.agents:
            filename.write("1 " + str(self.sim_step_num) + " " + str(agent.x * cell_size_cm) + " "
                           + str(agent.y * cell_size_cm) + "\n")
        filename.write("</data>\n")

    def calc_density_in_stats_area(self):
        num_of_agents = self.supervisor.num_of_agents_in_rect(*self.stats_area)
        density = num_of_agents/self.stats_area_size
        self.densities.append(density)

    def track_agents_in_stats_area(self):
        for agent in self.supervisor.agents:
            if agent.is_in_rect(*self.stats_area):
                agent.actualize_stats_area_time(self.sim_step_num)

    def calc_avg_density_in_frames(self, start, end):
        if not self.densities or start == end:
            return 0
        return mean(self.densities[start: end+1])
