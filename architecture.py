from modelParams import ModelParams

from pygame import Surface
from pygame import draw
from pygame import Rect

import numpy as np


class Architecture:

    def __init__(self, model_params: ModelParams):
        self.model_params = model_params
        with open(self.model_params.architecture_file) as f:
            resize_factor = int(self.model_params.architecture_cell_size / self.model_params.cell_size)
            self.height, self.width = [int(i) for i in next(f).split()]
            self.height *= resize_factor
            self.width *= resize_factor
            self.plan = np.zeros((self.height, self.width))
            y_count = 0
            for line in f:
                x_count = 0
                for n in line.split():
                    for i in range(resize_factor):
                        for j in range(resize_factor):
                            self.plan[resize_factor*y_count+i][resize_factor*x_count+j] = int(n)
                    x_count += 1
                y_count += 1

    def get_cell_neighbors(self, y, x):
        neighbors = []
        for y_move in (-1, 0, 1):
            if y + y_move < 0 or y + y_move >= self.height:
                continue
            else:
                for x_move in (-1, 0, 1):
                    if x + x_move < 0 or x + x_move >= self.width or (x_move == 0 and y_move == 0):
                        continue
                    else:
                        if self.plan[y+y_move][x+x_move] != self.model_params.wall:
                            neighbors.append((y + y_move, x + x_move))
        return neighbors

    def is_floor(self, y, x):
        if y >= self.height or x >= self.width or y < 0 or x < 0:
            return False
        if self.plan[y][x] == self.model_params.floor:
            return True
        return False

    def is_door(self, y, x):
        if self.plan[y][x] == self.model_params.door:
            return True
        return False

    def paint(self, screen: Surface):
        for y in range(self.height):
            for x in range(self.width):
                top_pos = self.model_params.cell_size_px * y
                left_pos = self.model_params.cell_size_px * x
                color = self.model_params.netting_color
                width = 1
                if self.plan[y][x] == self.model_params.wall:
                    color = self.model_params.wall_color
                    width = 0
                elif self.plan[y][x] == self.model_params.door:
                    color = self.model_params.door_color
                    width = 0
                draw.rect(screen, color, Rect(left_pos, top_pos, self.model_params.cell_size_px,
                                              self.model_params.cell_size_px), width)

