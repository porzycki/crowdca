import numpy as np
from mpl_toolkits.mplot3d import Axes3D # This import has side effects required for the kwarg projection='3d' in the call to fig.add_subplot
import matplotlib.pyplot as plt
from matplotlib import cm
import random
import math

# x4 - x3 + y2 + z2 = 0
def fun1(x, y):
    y_n = -y/1.5 + 0.8
    under = (y_n)**3 - (y_n)**4 - (x**2)
    if under > 0:
        return math.sqrt(under)
    else:
        return 0

#ellipsoid
def fun3 (x,y):
    a = 0.23
    b = 0.14
    c = 1.0
    under = c**2 - (x**2*c**2/a**2) - (y**2*c**2/b**2)
    if under > 0:
        return math.sqrt(c**2 - (x**2*c**2/a**2) - (y**2*c**2/b**2))
    else:
        return 0





x = y = np.arange(-1.5, 1.5, 0.01)
X, Y = np.meshgrid(x, y)

psychological = np.array([fun1(x, y) for x, y in zip(np.ravel(X), np.ravel(Y))])
physical = np.array([fun3(x, y) for x, y in zip(np.ravel(X), np.ravel(Y))])
total = np.array([max(fun1(x, y), fun3(x, y)) for x, y in zip(np.ravel(X), np.ravel(Y))])


Z = psychological.reshape(X.shape)

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot_surface(X, Y, Z, cmap=plt.get_cmap('jet'), vmax=1.0)

ax.set_xlabel('X Label')
ax.set_ylabel('Y Label')
ax.set_zlabel('Z Label')

plt.show()
