from agent import Agent
from modelParams import ModelParams

import unittest


class AgentInShapeUnitTest(unittest.TestCase):

    def setUp(self):
        self.modelParams = ModelParams()
        self.agt = Agent(10, 15, self.modelParams)

    def test_square_basic_true(self):
        """basic true scenario"""
        self.assertTrue(self.agt.is_in_rect(1, 1, 20, 20))

    def test_square_x_too_small(self):
        """agt x pos too small"""
        self.assertFalse(self.agt.is_in_rect(1, 16, 20, 20))

    def test_square_x_too_big(self):
        """agt x pos too big"""
        self.assertFalse(self.agt.is_in_rect(1, 1, 20, 10))

    def test_square_y_too_small(self):
        """agt y pos too small"""
        self.assertFalse(self.agt.is_in_rect(12, 1, 20, 20))

    def test_square_y_too_big(self):
        """check agent in rect"""
        self.assertFalse(self.agt.is_in_rect(1, 1, 8, 20))

    def test_square_top_corner(self):
        """agt on the corner of rect"""
        self.assertTrue(self.agt.is_in_rect(10, 15, 20, 20))

    def test_square_lower_corner(self):
        """agt on the corner of rect"""
        self.assertTrue(self.agt.is_in_rect(1, 15, 10, 20))

    def test_circle_basic(self):
        """basic check for agent in circle"""
        self.assertTrue(self.agt.is_in_circle(12, 12, 10))

    def test_circle_basic_false(self):
        """basic check for agent in circle"""
        self.assertFalse(self.agt.is_in_circle(12, 12, 1))

    def test_circle_edge(self):
        """basic check for agent in circle"""
        self.assertTrue(self.agt.is_in_circle(10, 10, 5))
