import matrixUtils
import numpy as np

import unittest


class MatrixPasteCorrectTest(unittest.TestCase):

    def setUp(self):
        self.first = np.array([[2.0, 2.0, 2.0], [2.0, 2.0, 2.0]])
        self.second = np.array([[1.0, 1.0], [1.0, 1.0]])

    def test_paste_zero_zero(self):
        """pasting matrix at [0,0]"""
        expected_result = np.array([[3.0, 3.0, 2.0], [3.0, 3.0, 2.0]])
        matrixUtils.paste(self.first, self.second, (0, 0))
        self.assertTrue(np.allclose(self.first, expected_result))

    def test_paste_moved_x(self):
        """pasting matrix moved in x"""
        expected_result = np.array([[2.0, 3.0, 3.0], [2.0, 3.0, 3.0]])
        matrixUtils.paste(self.first, self.second, (0, 1))
        self.assertTrue(np.allclose(self.first, expected_result))


if __name__ == "__main__":
    unittest.main()
