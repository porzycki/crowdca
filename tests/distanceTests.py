import matrixUtils

import unittest


class DistanceCalcResult(unittest.TestCase):
    def testZeroDistance(self):
        """Check zero distance if the same point is tested"""
        self.assertEqual(matrixUtils.distance(1, 1, 1, 1), 0.0)

    def testDistanceOne(self):
        """Check one step distance"""
        self.assertEqual(matrixUtils.distance(1, 1, 1, 2), 1.0)


if __name__ == "__main__":
    unittest.main()
