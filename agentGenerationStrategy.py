from enum import Enum


class AgentGenerationStrategy(Enum):

    FROM_FILE = 0
    RANDOM = 1
    RANDOM_IN_AREA = 2
    DURING_SIMULATION_IN_AREA = 3


