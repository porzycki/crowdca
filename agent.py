from floorField import FloorField
from modelParams import ModelParams
from direction import Direction
from scipy.ndimage.interpolation import rotate
import matrixUtils as mu

from pygame import Surface
from pygame import draw

import numpy as np
import math

import matplotlib.pyplot as plt


class Agent:

    def __init__(self, y, x, model_params: ModelParams, size=(0.45, 0.27)):
        # position and movement
        self.model_params = model_params
        self.y = y
        self.x = x
        self.desired_y = y
        self.desired_x = x
        self.last_direction = Direction.NONE
        self.diagonal_movement_counter = 0

        # size
        self.size_major_axis = size[0]
        self.size_minor_axis = size[1]
        self.agent_major_axis_px = int(self.size_major_axis * self.model_params.size_to_px)
        self.agent_minor_axis_px = int(self.size_minor_axis * self.model_params.size_to_px)

        # pff
        self.agent_pff_index = math.ceil(self.model_params.agent_pff_radius/self.model_params.cell_size) # in future x and y index
        self.prox_ff = self.calc_pfp_none()
        self.prox_ff_n = self.calc_pfp()
        self.prox_ff_nw = rotate(self.prox_ff_n, 45, order=1, reshape=False, prefilter=False)
        self.prox_ff_w = rotate(self.prox_ff_n, 90, order=1, reshape=False, prefilter=False)
        self.prox_ff_sw = rotate(self.prox_ff_n, 135, order=1, reshape=False, prefilter=False)
        self.prox_ff_s = rotate(self.prox_ff_n, 180, order=1, reshape=False, prefilter=False)
        self.prox_ff_se = rotate(self.prox_ff_n, 225, order=1, reshape=False, prefilter=False)
        self.prox_ff_e = rotate(self.prox_ff_n, 270, order=1, reshape=False, prefilter=False)
        self.prox_ff_ne = rotate(self.prox_ff_n, 315, order=1, reshape=False, prefilter=False)

        # pre-movement time params
        self.is_moving = False
        self.required_trigger = 0
        self.trigger_level = 0

        # injuries
        self.injury = 0
        self.is_injured = False

        # stats
        self.stats_area_enter = -1
        self.stats_area_leave = -1

    def get_pff(self):
        if self.last_direction == Direction.NONE:
            return self.prox_ff
        if self.last_direction == Direction.NORTH:
            return self.prox_ff_n
        if self.last_direction == Direction.NORTH_WEST:
            return self.prox_ff_nw
        if self.last_direction == Direction.WEST:
            return self.prox_ff_w
        if self.last_direction == Direction.SOUTH_WEST:
            return self.prox_ff_sw
        if self.last_direction == Direction.SOUTH:
            return self.prox_ff_s
        if self.last_direction == Direction.SOUTH_EAST:
            return self.prox_ff_se
        if self.last_direction == Direction.EAST:
            return self.prox_ff_e
        if self.last_direction == Direction.NORTH_EAST:
            return self.prox_ff_ne

    def calc_pfp_none(self):
        square_size = 2 * self.agent_pff_index + 1
        static_pff = np.zeros((square_size, square_size))
        for y in range(square_size):
            for x in range(square_size):
                x_cord = (x - self.agent_pff_index)*self.model_params.cell_size
                y_cord = (y - self.agent_pff_index)*self.model_params.cell_size
                static_pff[y][x] = self.fun3(x_cord, y_cord)
        return static_pff

    def calc_pfp(self):
        square_size = 2 * self.agent_pff_index + 1
        pff = np.zeros((square_size, square_size))
        for y in range(square_size):
            for x in range(square_size):
                x_cord = (x - self.agent_pff_index)*self.model_params.cell_size
                y_cord = (y - self.agent_pff_index)*self.model_params.cell_size
                pff[y][x] = max(self.fun1(x_cord, y_cord), self.fun3(x_cord, y_cord))
        return pff

    def fun1(self, x, y):
        y_n = y * 0.65 + 0.8 # wyśrodkowanie pola na agencie
        under = (y_n) ** 3 - (y_n) ** 4 - (x ** 2)
        if under > 0:
            return math.sqrt(under)
        else:
            return 0

    # ellipsoid
    def fun3(self, x, y):
        #a = 0.30
        #b = 0.2
        a = self.size_major_axis/2
        b = self.size_minor_axis/2
        c = 1.0
        under = c ** 2 - (x ** 2 * c ** 2 / a ** 2) - (y ** 2 * c ** 2 / b ** 2)
        if under > 0:
            return math.sqrt(c ** 2 - (x ** 2 * c ** 2 / a ** 2) - (y ** 2 * c ** 2 / b ** 2))
        else:
            return 0

    def calculate_injuries(self, ff: FloorField):
        self_pff = self.get_pff()
        pushing_level = ff.get_ff(self.y, self.x) - ff.get_sff(self.y, self.x) - self_pff[self.agent_pff_index][self.agent_pff_index]
        if pushing_level > self.model_params.min_pushing_to_injury:
            self.injury += pushing_level
            print("Agent", self,  " injury level: ",  self.injury)
        if self.injury > self.model_params.injury_limit:
            self.is_injured = True
            self.last_direction == Direction.NONE
            print("Agent is injured")

    def desired_move(self, ff: FloorField):
        if (not self.is_moving) or self.is_injured:
            self.desired_y = self.y
            self.desired_x = self.x
            return
        possible_moves = ff.arch.get_cell_neighbors(self.y, self.x)
        # actual_val = ff.get_ff(self.y, self.x)
        self_pff = self.get_pff()
        best_val = ff.get_ff(self.y, self.x) - self_pff[self.agent_pff_index][self.agent_pff_index]
        for y, x in possible_moves:
            pos_mov_val = ff.get_ff(y, x)
            pos_mov_val = round(pos_mov_val - self_pff[y-self.y+self.agent_pff_index][x-self.x+self.agent_pff_index], 5)  # usunięcie własnego wkładu w PFF
            if self.model_params.inertia:
                if self.last_direction == self.determine_movement_direction(x, y):
                    pos_mov_val = pos_mov_val - self.model_params.inertia_coef * self.model_params.cell_size
            if pos_mov_val < best_val:
                best_val = pos_mov_val
                self.desired_y, self.desired_x = y, x

    def determine_movement_direction(self, new_x, new_y):
        if self.y > new_y:
            if self.x > new_x:
                return Direction.NORTH_WEST
            elif self.x < new_x:
                return Direction.NORTH_EAST
            else:
                return Direction.NORTH
        elif self.y < new_y:
            if self.x > new_x:
                return Direction.SOUTH_WEST
            elif self.x < new_x:
                return Direction.SOUTH_EAST
            else:
                return Direction.SOUTH
        else:
            if self.x > new_x:
                return Direction.WEST
            elif self.x < new_x:
                return Direction.EAST
            else:
                return Direction.NONE

    def move(self):
        if not self.is_moving:
            return
        if self.model_params.diagonal_movement_reduction and self.diagonal_movement_counter > 1:
            self.diagonal_movement_counter -= 1
            return
        self.last_direction = self.determine_movement_direction(self.desired_x, self.desired_y)
        self.y, self.x = self.desired_y, self.desired_x
        if self.model_params.diagonal_movement_reduction and self.last_direction in (Direction.NORTH_EAST, Direction.SOUTH_EAST, Direction.NORTH_WEST, Direction.SOUTH_WEST):
            self.diagonal_movement_counter += 0.39
        #printing agents real position
        #print ("Agent pos: x= ", self.x * self.model_params.cell_size, " y= ", self.y * self.model_params.cell_size)

    def collide(self, other):
        if self.y == other.y and self.x == other.x:
            return True
        return False

    def will_collide(self, other):
        if self.desired_y == other.y and self.desired_x == other.x:
            return True
        return False

    # paint agent ellipse - static component of PFF
    def paint_agent_area(self, screen: Surface):
        half_size = int(self.model_params.cell_size_px / 2)
        surface = Surface(( self.agent_major_axis_px, self.agent_minor_axis_px))
        size = (0, 0,  self.agent_major_axis_px, self.agent_minor_axis_px)
        draw.ellipse(surface, self.model_params.agent_area_color, size)
        surface.set_colorkey((0, 0, 0))
        screen.blit(surface, (self.x * self.model_params.cell_size_px + half_size - self.agent_major_axis_px/2,
                              self.y * self.model_params.cell_size_px + half_size - self.agent_minor_axis_px/2))

    def paint(self, screen: Surface):
        # self.paint_agent_area(screen)
        color = self.model_params.agent_color
        if self.is_injured:
            color = self.model_params.injuried_agent_color
        draw.rect(screen, color,
                  (self.x * self.model_params.cell_size_px, self.y * self.model_params.cell_size_px,
                   self.model_params.cell_size_px, self.model_params.cell_size_px))


    def is_in_rect(self, y1, x1, y2, x2):
        if x1 <= self.x * self.model_params.cell_size <= x2 and y1 <= self.y * self.model_params.cell_size <= y2:
            return True
        return False

    def is_in_circle(self, y1, x1, radius):
        dist = mu.distance(y1, x1, self.y * self.model_params.cell_size, self.x * self.model_params.cell_size)
        if radius >= dist:
            return True
        return False

    def actualize_stats_area_time(self, sim_step_num):
        if self.stats_area_enter == -1:
            self.stats_area_enter = sim_step_num
        else:
            self.stats_area_leave = sim_step_num

    def decide_to_evacuate(self):
        if self.trigger_level >=self.required_trigger:
            self.is_moving = True
